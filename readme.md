# DSNY 201: Readme File Example

**Created/Revised:** Fall 2017

## Design Need and Goals

Put in a paragraph description about why this course was revised or created. For example: "This course in Muppet design was created to fill the need for electives in production design. Other courses created in this sequence include DSNY 202: Designing Better Sith Lords and DSNY 203: Superhero Hairstyles." 

If this course is a revision or based on an earlier course, note that here as well. 

The README.md file uses [markdown](https://daringfireball.net/projects/markdown/) format, which will be beautified by Bitbucket. 

## Design Team

* **Instructional Designer:** Name: email
* **Media Designer:** Name: email 
* **Writer:** Name: email
* **Subject Matter Expert:** Name: email 
* **Admin Approval:** Name: email

Add any additional design team roles as appropriate. Also note any team changes. 

## Notes

General design and implementation notes. 

## Version History

ADD TO TOP OF LIST 

### First-Run Changes: September 17, 2017

Changes to quizzes and assignments requested by teaching faculty (someone@scad.edu) and chair (someone@scad.edu). 

-- Rob Gillette 

### Admin Review: March 31, 2017

Additional readings requested by chair (someone@scad.edu).

-- Kirk Sluder

### QA Fixes: March 14, 2017

QA Fixes.

-- Kirk Sluder
