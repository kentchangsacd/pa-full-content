<%@ page extends = "edu.scad.elearning.contentserver.servlet.BaseJspServlet" import = "edu.scad.elearning.contentserver.util.TextHtmlUtil" %>
    <!doctype html>
    <html class="no-js" lang="">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Syllabus Page</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/main.css">
        <script src="../js/vendor/modernizr-2.8.3.min.js"></script>

        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="../css/materialize.min.css">



        <link rel="stylesheet" href="../css/layout.processed.css" />
        <link rel="stylesheet" href="../css/style.processed.css" />
    </head>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


        <header class="header valign-wrapper syllabus">
            <div class="header-bg">
                <img src="../assets/landing-images/banner-syllabus.svg" alt="" />
            </div>
            <div class="course-text">
                <div class="course-leading">
                    <div class="dept-title">Industrial Design</div>
                    <div class="school-title">School of Design</div>
                    <div class="course-id">PSYO 000</div>
                </div>
                <div class="course-title">
                    <h1>Psycho Aesthetics:</h1>
                    <h2>Advanced Design-thingking Methodology</h2>
                </div>
            </div>
            <div class="logo">
                <img src="../assets/icons/rks-scad-white.svg" alt="">
            </div>
        </header>

        <main class="main syllabus">
            <!-- OVERVIEW -->
            <section class="section overview">

                <div class="section-title-group">
                    <div class="section-icon">
                        <img src="../assets/icons/unit-1-overview.svg" alt="">
                    </div>
                    <div class="section-title">
                        <h1>Overview</h1>
                    </div>
                </div>

                <article class="section-article">
                    <p>
                        <%=getCourseDescription(request)%>
                    </p>
                </article>

            </section>

            <!-- GOALS & OUTCOMES -->
            <section class="section objectives">

                <div class="section-title-group">
                    <div class="section-icon">
                        <img src="../assets/icons/unit-1-objectives.svg" alt="">
                    </div>
                    <div class="section-title">
                        <h1>Objectives</h1>
                    </div>
                </div>

                <div class="section-card with-icon content-width">
                    <div class="card-icon">
                        <img src="../assets/icons/unit-1-objectives-during.svg" alt="">
                    </div>
                    <div class="card-content">
                        <%

    					String originalGoals = getSyllabusGoals(request);

    					out.println(TextHtmlUtil.formatSyllabusGoals(originalGoals));

    					%>
                    </div>
                </div>

                <div class="section-card with-icon content-width">
                    <div class="card-icon">
                        <img src="../assets/icons/unit-1-objectives-after.svg" alt="">
                    </div>
                    <div class="card-content">
                        <%

    					String originalOutcomes = getSyllabusOutcomes(request);

    					out.println(TextHtmlUtil.formatLearningOutcomes(originalOutcomes));

    					%>
                    </div>
                </div>

            </section>

            <!-- SCHEDULE			 -->
            <section class="section schedule">

                <div class="section-title-group">
                    <div class="section-icon">
                        <img src="../assets/icons/project-a-schedule.svg" alt="">
                    </div>
                    <div class="section-title">
                        <h1>Schedule</h1>
                    </div>
                </div>

                <article class="section-article schedule-collapsible">
                    <ul class="collapsible" data-collapsible="expandable">
                        <li>
                            <div class="collapsible-header">
                                <i class="material-icons">expand_more</i> Prequarter Assignment
                            </div>
                            <div class="collapsible-body">
                                <div class="body-part">
                                    <p>Before Day 1 of the quarter, read <em>Predictable Magic</em> in full. This text is integral to your ability to hit the ground running.</p>
                                    <p>Next, navigate to the course blog and use video recording software to create a persona of yourself, as you are now. Post your video, which will also serve as your introduction to your peers, and respond to at least
                                        two of your classmates by 11:59 p.m. U.S. EST/EDT on Day 4 of this unit.</p>
                                </div>
                            </div>
                        </li>
                        <!-- Unit 1 -->
                        <li>
                            <div class="collapsible-header">
                                <i class="material-icons">expand_more</i> Unit 1: <span class="title">Core Principles of Psycho-Aesthetics</span>
                            </div>
                            <div class="collapsible-body">
                                <div class="body-part">
                                    <h3>Topics</h3>
                                    <h4>The Psycho-Aesthetics Ideology</h4>
                                    <ul>
                                        <li>
                                            <p>The Importance of Collaboration</p>
                                        </li>
                                        <li>
                                            <p>The P/A Toolkit</p>
                                        </li>
                                    </ul>
                                    <h4>The Benefits of Psycho-Aesthetics</h4>
                                    <ul>
                                        <li>
                                            <p>Poor Collaboration</p>
                                        </li>
                                        <li>
                                            <p>Information Overload</p>
                                        </li>
                                    </ul>
                                    <h4>Contextual Research</h4>
                                    <ul>
                                        <li>
                                            <p>Persona and Benchmarking Research</p>
                                        </li>
                                        <li>
                                            <p>Collaboration and Contextual Research</p>
                                        </li>
                                        <li>
                                            <p>Sparking Conversation</p>
                                        </li>
                                    </ul>

                                </div>
                                <div class="body-part">
                                    <h3>Activities and Assignments Due</h3>

                                    <h4>Readings</h4>
                                    <p>Please read the following sections in your required texts:</p>
                                    <ul>
                                        <li>
                                            <p>RKS</p>
                                            <p><em>Psycho-Aesthetics: Advanced Design-Thinking Methodology</em></p>
                                            <p>Pages 1 &ndash; 17</p>
                                        </li>
                                        <li>
                                            <p>Elie Ofek and Michael Norris</p>
                                            <p><a href="javascript:popTheme('https://hbr.org/product/An-Exercise-in-Designing-/an/514042-PDF-ENG', '', 700);"><em>An Exercise in Designing a Travel Coffee Mug</em></a>.</p>
                                        </li>
                                    </ul>

                                    <h4>Video</h4>
                                    <p>Please watch the following video:</p>
                                    <ul>
                                        <li>
                                            <p>Ravi Sawhney, <a href="javascript:popTheme('https://www.youtube.com/watch?v=Tuc9DkM4k_k', '', 700);">&ldquo;Design,&rdquo;</a> TEDxConejo 2012.</p>
                                        </li>
                                    </ul>

                                    <h4>Discussions</h4>
                                    <ul>
                                        <li>
                                            <p>Project A, Part 1: Project Strategy, Peer Feedback</p>
                                        </li>
                                    </ul>

                                    <h4>Projects</h4>
                                    <ul>
                                        <li>
                                            <p>Project A, Part 1: Project Strategy</p>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </li>
                        <!-- Unit 2 -->
                        <li>
                            <div class="collapsible-header">
                                <i class="material-icons">expand_more</i> Unit 2: <span class="title">Shifting Through Contextual Research</span>
                            </div>
                            <div class="collapsible-body">
                                <div class="body-part">
                                    <h3>Topics</h3>
                                    <h4>Personas</h4>
                                    <ul>
                                        <li>
                                            <p>What Goes into a Persona?</p>
                                        </li>
                                        <li>
                                            <p>What Comes out of a Persona?</p>
                                        </li>
                                        <li>
                                            <p>Distilling Data into Clear Personas</p>
                                        </li>
                                        <li>
                                            <p>Future-State Persona</p>
                                        </li>
                                    </ul>
                                    <h4>Help-Me Statements and Key Triggers</h4>
                                    <ul>
                                        <li>
                                            <p>Help-Me Statements</p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="body-part">
                                    <h3>Activities and Assignments Due</h3>

                                    <h4>Readings</h4>
                                    <p>Please read the following sections in your required text:</p>
                                    <ul>
                                        <li>
                                            <p>RKS</p>
                                            <p><em>Psycho-Aesthetics: Advanced Design-Thinking Methodology</em></p>
                                            <p>Pages 18 &ndash; 31</p>
                                        </li>
                                    </ul>
                                    <p>Please review the following suggested readings:</p>
                                    <ul>
                                        <li>
                                            <p>AIGA, <a href="javascript:popTheme('https://www.scribd.com/doc/46873341/Ethnography-Primer', '', 700);"><em>An Ethnography Primer</em></a></p>
                                        </li>
                                        <li>
                                            <p>Natasha Mack et al., &ldquo;<a href="javascript:popTheme('http://www.ccs.neu.edu/course/is4800sp12/resources/qualmethods.pdf', '', 700);">Module 1: Qualitative Research Methods Overview,</a> &rdquo; in <em>Qualitative Research Methods: A Data Collector&rsquo;s Field Guide </em></p>
                                        </li>
                                    </ul>
                                    <p>For additional software support, please watch the following Lynda videos:</p>
                                    <ul>
                                        <li>
                                            <p><em><a href="javascript:popTheme('https://www.lynda.com/InDesign-tutorials/InDesign-CC-Essential-Training-2015/368575-2.html', '', 700);">InDesign CC 2015 Essential Training </a></em></p>
                                        </li>
                                        <li>
                                            <p><a href="javascript:popTheme('https://www.lynda.com/InDesign-tutorials/InDesign-Secrets/85324-2.html', '', 700);"><em>InDesign Secrets</em></a>.</p>
                                        </li>
                                    </ul>

                                    <h4>Discussions</h4>
                                    <ul>
                                        <li>
                                            <p>Project A, Part 2: Contextual Research and Personas, Peer Feedback</p>
                                        </li>
                                    </ul>

                                    <h4>Projects</h4>
                                    <ul>
                                        <li>
                                            <p>Project A, Part 2: Contextual Research and Personas</p>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </li>
                        <!-- Unit 3 -->
                        <li>
                            <div class="collapsible-header">
                                <i class="material-icons">expand_more</i> Unit 3: <span class="title">The P/A Map</span>
                            </div>
                            <div class="collapsible-body">
                                <div class="body-part">
                                    <h3>Topics</h3>
                                    <h4>Structure of the P/A Map</h4>
                                    <ul>
                                        <li>
                                            <p>Psycho-Aesthetics Mapping</p>
                                        </li>
                                        <li>
                                            <p>Using the P/A Map</p>
                                        </li>
                                        <li>
                                            <p>The P/A Map and Collaboration</p>
                                        </li>
                                        <li>
                                            <p>The Map as an Anchor</p>
                                        </li>
                                    </ul>
                                    <h4>Identifying Opportunity Zones</h4>
                                    <h4>Key Attractors</h4>
                                    <h4>Design Ideation</h4>
                                </div>
                                <div class="body-part">
                                    <h3>Activities and Assignments Due</h3>

                                    <h4>Readings</h4>
                                    <p>Please read the following sections in your required text:</p>
                                    <ul>
                                        <li>
                                            <p>RKS</p>
                                            <p><em>Psycho-Aesthetics: Advanced Design-Thinking Methodology</em></p>
                                            <p>Pages 32 &ndash; 49</p>
                                        </li>
                                    </ul>

                                    <h4>Discussions</h4>
                                    <ul>
                                        <li>
                                            <p>Project A, Part 3: Mapping the Future and Locating Opportunity Zones, Peer Feedback</p>
                                        </li>
                                    </ul>

                                    <h4>Projects</h4>
                                    <ul>
                                        <li>
                                            <p>Project A, Part 3: Mapping the Future and Locating Opportunity Zones</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <!-- Unit 4 -->
                        <li>
                            <div class="collapsible-header">
                                <i class="material-icons">expand_more</i> Unit 4: <span class="title">The Hero&rsquo;s Journey</span>
                            </div>
                            <div class="collapsible-body">
                                <div class="body-part">
                                    <h3>Topics</h3>
                                    <h4>The Hero&rsquo;s Journey</h4>
                                    <ul>
                                        <li>
                                            <p>Taking the Design through the Hero&rsquo;s Journey</p>
                                        </li>
                                        <li>
                                            <p>The Importance of Catching Mistakes</p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="body-part">
                                    <h3>Activities and Assignments Due</h3>

                                    <h4>Readings</h4>
                                    <p>Please read the following sections in your required text:</p>
                                    <ul>
                                        <li>
                                            <p>RKS</p>
                                            <p><em>Psycho-Aesthetics: Advanced Design-Thinking Methodology</em></p>
                                            <p>Pages 50 &ndash; 61</p>
                                        </li>
                                    </ul>

                                    <h4>Discussions</h4>
                                    <ul>
                                        <li>
                                            <p>Project A, Part 4: Key Attractors and the Hero&rsquo;s Journey, Peer Feedback</p>
                                        </li>
                                    </ul>

                                    <h4>Projects</h4>
                                    <ul>
                                        <li>
                                            <p>Project A, Part 4: Key Attractors and the Hero&rsquo;s Journey</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <!-- Unit 5 -->
                        <li>
                            <div class="collapsible-header">
                                <i class="material-icons">expand_more</i> Unit 5: <span class="title">Process Presentation</span>
                            </div>
                            <div class="collapsible-body">
                                <div class="body-part">
                                    <h3>Topics</h3>
                                    <h4>Design Execution</h4>
                                    <h4>Articulating Your Decisions</h4>
                                    <ul>
                                        <li>
                                            <p>Context and Audience</p>
                                        </li>
                                        <li>
                                            <p>Packaging Your Work</p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="body-part">
                                    <h3>Activities and Assignments Due</h3>

                                    <h4>Readings</h4>
                                    <p>Please read the following sections in your required text:</p>
                                    <ul>
                                        <li>
                                            <p>RKS</p>
                                            <p><em>Psycho-Aesthetics: Advanced Design-Thinking Methodology</em></p>
                                            <p>Pages 62 &ndash; 77</p>
                                        </li>
                                    </ul>
                                    <p>For additional software support, please watch the following Lynda videos:</p>
                                    <ul>
                                        <li>
                                            <p><em><a href="javascript:popTheme('https://www.lynda.com/After-Effects-tutorials/Getting-Started-After-Effects-CC-2015/437367-2.html', '', 700);">Learning After Effects CC 2015</a></em></p>
                                        </li>
                                        <li>
                                            <p><em><a href="javascript:popTheme('https://www.lynda.com/After-Effects-tutorials/After-Effects-CC-Essential-Training-2015/371399-2.html', '', 700);">After Effects CC 2015 Essential Training.</a></em></p>
                                        </li>
                                    </ul>

                                    <h4>Discussions</h4>
                                    <ul>
                                        <li>
                                            <p>Project A, Part 5: Final Product Presentation, Peer Feedback and Final Submission</p>
                                        </li>
                                    </ul>

                                    <h4>Projects</h4>
                                    <ul>
                                        <li>
                                            <p>Project A, Part 5: Final Product Presentation</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <!-- Unit 6 -->
                        <li>
                            <div class="collapsible-header">
                                <i class="material-icons">expand_more</i> Unit 6: <span class="title">Laying the Foundation</span>
                            </div>
                            <div class="collapsible-body">
                                <div class="body-part">
                                    <h3>Topics</h3>
                                    <h4>Contextual Research</h4>
                                    <ul>
                                        <li>
                                            <p>Tips for Conducting Contextual Research</p>
                                        </li>
                                    </ul>
                                    <h4>P/A Applications: Product</h4>
                                </div>
                                <div class="body-part">
                                    <h3>Activities and Assignments Due</h3>

                                    <h4>Readings</h4>
                                    <p>Please review the following suggested readings:</p>
                                    <ul>
                                        <li>
                                            <p>William Badke, <em>Research Strategies: Finding Your Way Through the Information Fog</em>, 5th ed. (iUniverse LLC, 2014)</p>
                                            <p>Chapter 2</p>
                                        </li>
                                        <li>
                                            <p>Joseph Campbell, <em>The Hero with a Thousand Faces (The Collected Works of Joseph Campbell)</em>, 3<sup>rd</sup> (New World Library, 2008)</p>
                                            <p>Chapter III, Transformations of the Hero</p>
                                        </li>
                                    </ul>

                                    <h4>Discussions</h4>
                                    <ul>
                                        <li>
                                            <p>Project B, Part 1: Ideation and Project Strategy Statement, Peer Feedback</p>
                                        </li>
                                    </ul>

                                    <h4>Projects</h4>
                                    <ul>
                                        <li>
                                            <p>Project B, Part 1: Ideation and Project Strategy Statement</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <!-- Unit 7 -->
                        <li>
                            <div class="collapsible-header">
                                <i class="material-icons">expand_more</i> Unit 7: <span class="title">Extracting Value from Contextual Research</span>
                            </div>
                            <div class="collapsible-body">
                                <div class="body-part">
                                    <h3>Topics</h3>
                                    <h4>Interpreting Contextual Research</h4>
                                    <ul>
                                        <li>
                                            <p>Persona Development</p>
                                        </li>
                                    </ul>
                                    <h4>P/A Applications: Service</h4>
                                </div>
                                <div class="body-part">
                                    <h3>Activities and Assignments Due</h3>

                                    <h4>Readings</h4>
                                    <p>Please review the following suggested readings:</p>
                                    <ul>
                                        <li>
                                            <p>James Kalbach, <em>Mapping Experiences: A Complete Guide to Creating Value through Journeys, Blueprints, and Diagrams </em>(O&rsquo;Reilly Media, 2016)</p>
                                            <p>Part 3</p>
                                        </li>
                                        <li>
                                            <p>Nora Herting and Heather Willems,<em> Draw Your Big Idea: The Ultimate Creativity Tool for Turning Thoughts into Action and Dreams into Reality! </em>(Chronicle Books, 2016)</p>
                                        </li>
                                        <li>
                                            <p>Isabel Meirelles, <a href="javascript:popTheme('http://isabelmeirelles.com/book-design-for-information/', '', 700);"><em>Design for Information: An Introduction to the Histories, Theories, and Best Practices Behind Effective Information Visualizations</em></a>                                                (Rockport Publishers, 2013)</p>
                                            <p>Introduction.</p>
                                        </li>
                                    </ul>

                                    <h4>Discussions</h4>
                                    <ul>
                                        <li>
                                            <p>Project B, Part 2: Contextual Research and Personas, Peer Feedback</p>
                                        </li>
                                    </ul>

                                    <h4>Projects</h4>
                                    <ul>
                                        <li>
                                            <p>Project B, Part 2: Contextual Research and Personas</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <!-- Unit 8 -->
                        <li>
                            <div class="collapsible-header">
                                <i class="material-icons">expand_more</i> Unit 8: <span class="title">Visualizing Research</span>
                            </div>
                            <div class="collapsible-body">
                                <div class="body-part">
                                    <h3>Topics</h3>
                                    <h4>Mapping Effectively</h4>
                                    <ul>
                                        <li>
                                            <p>Identifying Opportunity Zones</p>
                                        </li>
                                    </ul>
                                    <h4>P/A Applications: Brand</h4>
                                </div>
                                <div class="body-part">
                                    <h3>Activities and Assignments Due</h3>

                                    <h4>Readings</h4>
                                    <p>Please review the following suggested readings:</p>
                                    <ul>
                                        <li>
                                            <p>Lucy Kimbell, <a href="javascript:popTheme('https://serviceinnovationhandbook.org/', '', 700);"><em>The Service Innovation Handbook: Action-Oriented Creative Thinking Toolkit for Service Organizations</em></a>                                                (BIS Publishers, 2015)</p>
                                            <p>Chapters 2 and 3.</p>
                                        </li>
                                    </ul>

                                    <h4>Discussions</h4>
                                    <ul>
                                        <li>
                                            <p>Project B, Part 3: Mapping and Key Attractors, Peer Feedback</p>
                                        </li>
                                    </ul>

                                    <h4>Projects</h4>
                                    <ul>
                                        <li>
                                            <p>Project B, Part 3: Mapping and Key Attractors</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <!-- Unit 9 -->
                        <li>
                            <div class="collapsible-header">
                                <i class="material-icons">expand_more</i> Unit 9: <span class="title">Testing via Storytelling</span>
                            </div>
                            <div class="collapsible-body">
                                <div class="body-part">
                                    <h3>Topics</h3>
                                    <h4>The Hero&rsquo;s Journey</h4>
                                    <ul>
                                        <li>
                                            <p>Honing in on a Design Solution</p>
                                        </li>
                                    </ul>
                                    <h4>P/A Applications: Business</h4>
                                </div>
                                <div class="body-part">
                                    <h3>Activities and Assignments Due</h3>

                                    <h4>Readings</h4>
                                    <p>Please review the following suggested readings:</p>
                                    <ul>
                                        <li>
                                            <p>Joseph Campbell, <em>The Hero with a Thousand Faces (The Collected Works of Joseph Campbell)</em>, 3<sup>rd</sup> (New World Library, 2008)</p>
                                            <p>Chapter III, &ldquo;Transformations of the Hero&rdquo;</p>
                                        </li>
                                    </ul>

                                    <h4>Discussions</h4>
                                    <ul>
                                        <li>
                                            <p>Project B, Part 4: The Hero&rsquo;s Journey, Peer Feedback</p>
                                        </li>
                                    </ul>

                                    <h4>Projects</h4>
                                    <ul>
                                        <li>
                                            <p>Project B, Part 4: The Hero&rsquo;s Journey</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <!-- Unit 10 -->
                        <li>
                            <div class="collapsible-header">
                                <i class="material-icons">expand_more</i> Unit 10: <span class="title">Psycho-Aesthetics and Future Endeavors</span>
                            </div>
                            <div class="collapsible-body">
                                <div class="body-part">
                                    <h3>Activities and Assignments Due</h3>

                                    <h4>Discussions</h4>
                                    <ul>
                                        <li>
                                            <p>Project B, Part 5: Final Product Presentation, Peer Feedback and Final Submission</p>
                                        </li>
                                    </ul>

                                    <h4>Projects</h4>
                                    <ul>
                                        <li>
                                            <p>Project B, Part 5: Final Product Presentation</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>


                    </ul>
                </article>

            </section>

            <!-- TEXTBOOKs -->
            <section class="section textbooks">
                <div class="section-title-group">
                    <div class="section-icon">
                        <img src="../assets/icons/syllabus-textbook_textbook.svg" alt="">
                    </div>
                    <div class="section-title">
                        <h1>Textbooks and Supplies</h1>
                    </div>
                </div>

                <div class="section-card with-icon content-width">
                    <div class="card-icon">
                        <img src="../assets/icons/todo-1-readings.svg" alt="">
                    </div>
                    <div class="card-content">
                        <h4>Required Texts:</h4>
                        <ul>
                            <li>
                                <p>Ofek, Elie, and Michael Norris</p>
                                <p><a href="javascript:popTheme('https://hbr.org/product/An-Exercise-in-Designing-/an/514042-PDF-ENG', '', 700);"><em>An Exercise in Designing a Travel Coffee Mug</em></a>.</p>
                                <p><em>Harvard Business Review</em>.</p>
                            </li>
                            <li>
                                <p>Prahalad, Deepa, and Ravi Sawhney</p>
                                <p><em>Predictable Magic: Unleash the Power of Design Strategy to Transform Your Business</em>.</p>
                                <p>FT Press, 2010. ISBN: 978-0137023486.</p>
                            </li>
                            <li>
                                <p>RKS</p>
                                <p><em>Psycho-Aesthetics: Advanced Design-Thinking Methodology. </em>RKS Design, 2016.</p>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="section-card with-icon content-width">
                    <div class="card-icon">
                        <img src="../assets/icons/todo-1-readings.svg" alt="">
                    </div>
                    <div class="card-content">
                        <h4>Suggested Texts:</h4>
                        <ul>
                            <li>
                                <p>William Badke</p>
                                <p><em>Research Strategies: Finding Your Way Through the Information Fog</em>, 5th ed.</p>
                            </li>
                            <li>
                                <p>Joseph Campbell</p>
                                <p><em>The Hero with a Thousand Faces (The Collected Works of Joseph Campbell)</em>, 3<sup>rd</sup> ed.</p>
                            </li>
                            <li>
                                <p>Nora Herting and Heather Willems</p>
                                <p><em>Draw Your Big Idea: The Ultimate Creativity Tool for Turning Thoughts into Action and Dreams into Reality!</em></p>
                            </li>
                            <li>
                                <p>James Kalbach</p>
                                <p><em>Mapping Experiences: A Complete Guide to Creating Value through Journeys, Blueprints, and Diagrams</em></p>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="section-card with-icon content-width">
                    <div class="card-icon">
                        <img src="../assets/icons/project-b-schedule-materials.svg" alt="">
                    </div>
                    <div class="card-content">
                        <h4>Required Supplies</h4>
                        <p>Many of the materials necessary will depend on the nature of your particular project, but students will need at least the following:</p>
                        <ul>
                            <li>
                                <p>Adobe Creative Cloud (<a href="javascript:popTheme('https://http://myscad.scad.edu/', '', 700);">available through MySCAD</a>)</p>
                            </li>
                            <li>
                                <p>Microsoft 365 (<a href="javascript:popTheme('https://http://myscad.scad.edu/', '', 700);">available through MySCAD</a>)</p>
                            </li>
                            <li>
                                <p>screen or video recording software (available through the online classroom).</p>
                            </li>
                        </ul>
                    </div>
                </div>

            </section>

            <!-- GRADING			 -->

            <section class="section grading">
                <div class="section-title-group">
                    <div class="section-icon">
                        <!-- 						Put the icon here          -->
                    </div>
                    <div class="section-title">
                        <h1>Grading</h1>
                    </div>
                </div>

                <article class="section-article">
                    <p>Your overall course grade will be computed according to the following breakdown:</p>
                    <div class="grade-table">
                        <div class="grade-item grade-25">
                            <div class="item-name">Discussions</div>
                            <div class="item-weight">25%</div>
                        </div>
                        <div class="grade-item grade-25">
                            <div class="item-name">Project A</div>
                            <div class="item-weight">25%</div>
                        </div>
                        <div class="grade-item grade-50">
                            <div class="item-name">Project B</div>
                            <div class="item-weight">50%</div>
                        </div>
                    </div>
                </article>

            </section>

            <!-- FACAULTY -->
            <section class="section faculty">

                <div class="section-title-group">
                    <div class="section-icon">
                        <!-- 						Put the icon here          -->
                    </div>
                    <div class="section-title">
                        <h1>Faculty Information</h1>
                    </div>
                </div>
                <%=getFacultyPage(request)%>

            </section>

            <!-- GENERAL INFORMATION -->
            <section class="section general-info">

                <div class="section-title-group">
                    <div class="section-icon">
                        <!-- 						Put the icon here          -->
                    </div>
                    <div class="section-title">
                        <h1>General Information</h1>
                    </div>
                </div>

                <article class="section-article schedule-collapsible">
                    <ul class="collapsible" data-collapsible="expandable">
                        <li>
                            <div class="collapsible-header">
                                <i class="material-icons">expand_more</i> Policies
                            </div>
                            <div class="collapsible-body">
                                <div class="body-part">
                                    <h2> Student Access </h2>
                                    <p> Courses will be available to students one week before the start of the quarter and for 10 days after the end of the quarter.</p>
                                    <h2>Email</h2>
                                    <p>Students must use their SCAD email accounts for all correspondence with their professors. This will help ensure that email is secure and that SCAD staff can assist students with email related technical problems. SCAD&rsquo;s
                                        Technology Support Center staff will not be able help with problems involving external email services, such as Yahoo! Mail, Gmail, or Hotmail.</p>
                                    <h2>Attendance</h2>
                                    <p>The SCAD Attendance Policy is published in the SCAD catalog, website, student handbook, and every course. SCAD faculty take attendance according to this policy for all classes, online and onsite to determine students&rsquo;
                                        attendance status. Please review the full SCAD<strong> Attendance Policy</strong> for a description of all guidelines and requirements. </p>
                                    <h2>Academic Integrity</h2>
                                    <p>Under all circumstances, students are expected to be honest in their dealings with faculty, administrative staff and other students. For purposes of this policy, the term faculty or faculty member includes any person
                                        engaged by the university to act in a teaching capacity, regardless of the person's actual title.</p>
                                    <p>In speaking with members of the SCAD community, students must give an accurate representation of the facts at hand. Failure to do so is considered a breach of the Student Code of Conduct and may result in sanctions
                                        against the student, including suspension or dismissal.</p>
                                    <p>In class assignments, students must submit work that fairly and accurately reflects their level of accomplishment. Any work that is not a product of the student's own efforts is considered dishonest. Students must not
                                        engage in academic dishonesty; doing so can have serious consequences. Academic dishonesty includes, but is not limited to, the following:</p>
                                    <ul>
                                        <li>
                                            <p>Cheating, which includes, but is not limited to, (a) the giving or receiving of any unauthorized assistance in producing assignments or taking quizzes, tests or examinations; (b) dependence on the aid of sources
                                                including technology beyond those authorized by the instructor in writing papers, preparing reports, solving problems or carrying out other assignments; (c) the acquisition, without permission, of tests
                                                or other academic material belonging to a member of the university faculty or staff; or (d) the use of unauthorized assistance in the preparation of works of art.</p>
                                        </li>
                                        <li>
                                            <p>Plagiarism, which includes, but is not limited to, the use, by paraphrase or direct quotation, of the published or unpublished work of another person without full and clear acknowledgment. Plagiarism also includes
                                                the unacknowledged use of materials prepared by another person or agency engaged in the selling of term papers or other academic materials.</p>
                                        </li>
                                        <li>
                                            <p>Submission of the same work in two or more classes without prior written approval of the professors of the classes involved.</p>
                                        </li>
                                        <li>
                                            <p>Submission of any work not actually produced by the student submitting the work without full and clear written acknowledgement of the actual author or creator of the work.</p>
                                        </li>
                                    </ul>
                                    <p>Please refer to the SCAD Student Handbook and view the <strong>Academic Integrity for Success</strong> presentation for additional information regarding the policy on academic integrity. </p>
                                    <h2>Student Surveys</h2>
                                    <p>Students are strongly encouraged to provide feedback on their university experience through SCAD's institutional surveys. The SCAD Student Survey and the Priorities Survey for Online Learners will both be administered
                                        in spring quarter. SCAD Student Survey will be emailed to each student account starting in Week 1 and will remain open through Week 4. The Priorities Survey for Online Learners will be emailed to every student’s
                                        account starting in Week 5 and will remain open through Week 8. SCAD's office of institutional effectiveness is responsible for gathering and delivering survey results to decision-makers on campus. For more information
                                        or questions, contact us at surveys@scad.edu.

                                        <h2>TEACH Act</h2>
                                        <p>The materials in an online course are strictly for the use of students enrolled in the course for purposes associated with the course and may not be retained or further disseminated. Students should acknowledge
                                            their understanding of TEACH Act copyright. Please read and respond to the Copyright Notice, which is accessible via the course menu.</p>

                                        <h2>Americans with Disabilities Act</h2>
                                        <p>In compliance with the Americans with Disabilities Act of 1990 (ADA) and Section 504 of the Rehabilitation Act of 1973, SCAD offers integrated educational services to assist students with disabilities to obtain
                                            a college education. For more information on services for students with disabilities, please contact the coordinator of Disability Services at 912.525.4665.</p>

                                        <h2>Evacuation Information</h2>
                                        <p>Students in programs that include visits, meetings, or residencies on a SCAD campus should familiarize themselves with the most appropriate route to emergency exits in classroom buildings. Emergency exits are indicated
                                            on charts posted in classrooms, hallways, or stairwells. Should a drill or emergency occur that would require evacuation, a class is to meet in a predetermined location away from the building. Professors will
                                            take attendance in an effort to account for all students.</p>

                                        <h2>College Identification</h2>
                                        <p>Students meeting face-to-face or participating in residencies on a SCAD campus will need a valid SCAD ID and photo ID to enter the college buildings. This procedure is intended to help ensure the safety of all students
                                            and college personnel at SCAD.</p>

                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="collapsible-header">
                                <i class="material-icons">expand_more</i> Computing Requirements
                            </div>
                            <div class="collapsible-body">
                                <div class="body-part">

                                    <p>To access online courses, you must use a multimedia-class computer with Internet connectivity. Minimum system requirements are described below.</p>
                                    <h2>Required Hardware</h2>
                                </div>
                                <div class="body-part">
                                    <div class="systemrequirements" style="width:550px; float:none;">
                                        <h3>PC</h3>
                                        <table width="550" height="144" border="1">
                                            <tr>
                                                <th width="185" align="left" scope="col">&nbsp;</th>
                                                <th width="180" scope="col">Minimum</th>
                                                <th width="185" scope="col">Suggested</th>
                                            </tr>
                                            <tr>
                                                <th align="left" scope="row">Processor</th>
                                                <td>Intel Core i3</td>
                                                <td>Intel Core i7</td>
                                            </tr>
                                            <tr>
                                                <th align="left" scope="row">OS</th>
                                                <td>Windows 7</td>
                                                <td>Windows 7</td>
                                            </tr>
                                            <tr>
                                                <th align="left" scope="row">RAM</th>
                                                <td>2GB</td>
                                                <td>4GB</td>
                                            </tr>
                                            <tr>
                                                <th align="left" scope="row">Disk Space</th>
                                                <td>160GB</td>
                                                <td>500GB</td>
                                            </tr>
                                            <tr>
                                                <th align="left" scope="row">Connection</th>
                                                <td>Broadband</td>
                                                <td>Broadband</td>
                                            </tr>
                                            <tr>
                                                <th align="left" scope="row">Communication</th>
                                                <td>N/A</td>
                                                <td>Webcam and microphone</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="body-part">
                                    <div class="systemrequirements" style="width:550px; float:none;">
                                        <h3>Max OS</h3>
                                        <table width="550" height="144" border="1">
                                            <tr>
                                                <th width="185" align="left" scope="col">&nbsp;</th>
                                                <th width="180" scope="col">Minimum</th>
                                                <th width="185" scope="col">Suggested</th>
                                            </tr>
                                            <tr>
                                                <th align="left" scope="row">Processor</th>
                                                <td>Intel-based iMac</td>
                                                <td>Intel-based Mac Pro</td>
                                            </tr>
                                            <tr>
                                                <th align="left" scope="row">OS</th>
                                                <td>OS 10.6</td>
                                                <td>OS 10.6</td>
                                            </tr>
                                            <tr>
                                                <th align="left" scope="row">RAM</th>
                                                <td>2GB</td>
                                                <td>8GB</td>
                                            </tr>
                                            <tr>
                                                <th align="left" scope="row">Disk Space</th>
                                                <td>160GB</td>
                                                <td>500GB</td>
                                            </tr>
                                            <tr>
                                                <th align="left" scope="row">Connection</th>
                                                <td>Broadband</td>
                                                <td>Broadband</td>
                                            </tr>
                                            <tr>
                                                <th align="left" scope="row">Communication</th>
                                                <td>N/A</td>
                                                <td>Webcam and microphone</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="body-part">
                                    <p>Please note that this course may require the use of software that has additional system requirements. Check the <strong>Textbooks and Supplies</strong> page of the <strong>Syllabus</strong> for additional software requirements,
                                        and consult the system requirements listed by the software manufacturer for appropriate hardware configurations.</p>
                                    <p>As a student at SCAD, you have free access to tutorials on Lynda.com. When accessing a Lynda.com tutorial directly from Blackboard, use the steps outlined in the video below:</p>
                                    <iframe src="https://cdnapisec.kaltura.com/p/1723081/sp/172308100/embedIframeJs/uiconf_id/30077681/partner_id/1723081?iframeembed=true&playerId=kaltura_player_1461249144&entry_id=1_jy3vj0z0" width="560" height="315" allowfullscreen webkitallowfullscreen
                                        mozAllowFullScreen frameborder="0" style="width: 560px; height: 315px;" itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
                <span itemprop="name" content="Accessing Lynda"></span>
                <span itemprop="description" content=""></span>
                <span itemprop="duration" content="88"></span>
                <span itemprop="thumbnail" content="https://cfvod.kaltura.com/p/1723081/sp/172308100/thumbnail/entry_id/1_jy3vj0z0/version/100021/acv/111"></span>
                <span itemprop="width" content="560"></span>
                <span itemprop="height" content="315"></span>
                </iframe>
                                    <h2>Required Browser</h2>
                                    <p>You must have an Internet browser installed on your computer to view and interact with online courses. Mozilla Firefox, version 4 or better, is recommended. Please note that your browser may need additional plug-ins
                                        or ancillary players to view courses that use multimedia.</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="collapsible-header">
                                <i class="material-icons">expand_more</i> Educational-Technologies Support
                            </div>
                            <div class="collapsible-body">
                                <div class="body-part">

                                    <h2>Blackboard</h2>
                                    <p>The Savannah College of Art and Design (SCAD) delivers SCAD eLearning courses via a Web-based learning-management system called Blackboard.</p>
                                    <p>If you are new to Blackboard or SCAD eLearning, take some time to explore the system. The best way to do this is to review the SCAD eLearning Resource Guide and to look through any online courses in which you are currently
                                        enrolled. Click the navigation buttons and observe the results. Note how content is organized, and learn how to use the system tools for communicating and for submitting assignments. </p>
                                    <p>For additional information and assistance, review SCAD eLearning's Frequently Asked Questions (F.A.Q.) and the eLearning contact information available when you click the Help button at the top of this window.</p>
                                    <p>SCAD eLearning staff members will help troubleshoot any performance issues you experience with Blackboard. However, issues that are related to your personal computer, your Internet Service Provider (ISP), or software
                                        specific to a course will need to be referred to a third-party support service. </p>
                                    <h2>Technology Support Center</h2>
                                    <p>If you need technical assistance not related to Blackboard or SCAD eLearning courses, contact the SCAD Technology Support Center. For the center&rsquo;s contact information and hours of operation, please see the
                                        <a href="http://myscad.scad.edu/scaddocs/departments/imt/services/tech_ops/tech_supportctr/index.cfm" onClick="if (window.open) { window.open('http://myscad.scad.edu/scaddocs/departments/imt/services/tech_ops/tech_supportctr/index.cfm', 'popup', 'scrollbars,resizable,width=800,height=600'); return false; }">Technology Support Center site in MySCAD</a>.</p>

                                    <h2>Learning Support</h2>

                                    <p>SCAD provides a number of services to assist students with learning needs related to course requirements and other activities. These services are available to all SCAD students. For more information, see the<a href="javascript:popTheme('http://myscad.scad.edu/scaddocs/departments/elearning/index.cfm', '', 700);"
                                            onClick="if (window.open) { window.open('http://myscad.scad.edu/scaddocs/departments/elearning/index.cfm', 'popup', 'scrollbars,resizable,width=800,height=600'); return false; }"> SCAD eLearning  site in MySCAD</a>.
                                        Anyone taking online courses at SCAD may also contact the office of online student success at <a href="mailto:ecampus@scad.edu">ecampus@scad.edu</a> for assistance with identifying appropriate services.</p>

                                </div>
                            </div>
                        </li>
                    </ul>
                </article>

            </section>

        </main>

        <footer class="footer valign-wrapper">
            <div class="footer-logo center-algin">
                <img src="../assets/icons/rks-scad-white.svg" alt="">
            </div>
        </footer>

        <script src="../js/vendor/jquery-1.12.0.min.js"></script>
        <script>
            window.jQuery || document.write('<script src="../js/vendor/jquery-1.12.0.min.js"><\/script>')
        </script>
        <!-- Compiled and minified JavaScript -->
        <script src="../js/vendor/materialize.min.js"></script>

        <script src="../js/plugins.js"></script>
        <!--         <script src="js/main.processed.js"></script> -->
        <!-- 			<script>
				$(document).ready(function(){
					$('ul.tabs').tabs(
						// {'swipeable':true}
					);
				});
			</script> -->
        <script>
            $('.collapsible-header').on('click', function() {
                console.log('open');
                window.setTimeout(function() {
                    parent.iframeResize();
                }, 500);
            });
            // $('.collapsible').collapsible({
            //   accordion: true, // A setting that changes the collapsible behavior to expandable instead of the default accordion style
            //   onOpen: function(el) {
            //     console.log(el);
            //     window.setTimeout(function(){
            //       parent.iframeResize();
            //     }, 500);
            //   }, // Callback for Collapsible open
            //   onClose: function(el) { alert('Closed'); } // Callback for Collapsible close
            // });
        </script>

        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-37168151-1']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();
        </script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                    function() {
                        (b[l].q = b[l].q || []).push(arguments)
                    });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = 'https://www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-XXXXX-X', 'auto');
            ga('send', 'pageview');
        </script>
    </body>

    </html>