var KPlayer = (function() {
  function KPlayer() {
    var _this = this;
    this.nameList = {};
    // $('#theater').hide()
    // console.log("loaded")
    var targetVideos = $('.video-container');
    var triggers = $('.video-thumb');
    // ## create objects
    for (var _i = 0, targetVideos_1 = targetVideos; _i < targetVideos_1.length; _i++) {
      var item = targetVideos_1[_i];
      var vid = $(item).attr('vid');
      this.nameList[vid] = $(item)[0];
    }
    var _loop_1 = function(i) {
      $(triggers[i]).on('click', function() {
        var vid = $(triggers[i]).attr('vid');
        $(_this.nameList[vid]).addClass('active');
        // $('#theater').addClass('show')
        $('#theater').fadeIn('slow');
        // console.log("clicked" + vid);
      });
    };
    // console.log(this.nameList);
    // ## assign actions
    for (var i = 0; i < triggers.length; i++) {
      _loop_1(i);
    }
    // ## assign closing actions
    $('#theater #theater-btn-close').on('click', function() {
      $('#theater').find('.active').removeClass('active');
      $('#theater').fadeOut('slow');
    });
  }
  return KPlayer;
}());
var PAStickyMenu = (function() {
  function PAStickyMenu(targetID) {
    this.isActive = false;
    this.host = $('#' + targetID);
    this.navHeader = this.host.find('.sticky-nav');
    this.navContainer = this.host.find('.nav-container');
    this.hostTop = this.host.position().top;
    this.hostH = this.host.height();
    this.threshold = this.hostTop + this.hostH;
    // console.log(this.hostTop + ' / ' + this.hostH)
    var footer = $('footer');
    if (footer.length > 0) {
      this.footerTop = $(footer).position().top;
    }
    this._assignActions();
  }
  PAStickyMenu.prototype._updateFooterTop = function() {
    var footer = $('footer');
    this.footerTop = $(footer).position().top;
  };
  PAStickyMenu.prototype._assignActions = function() {
    var _this = this;
    // scrolling
    var windowH = $(window).height();
    $(window).scroll(function() {
      var $nav = $(_this.host);
      var $navMenu = $nav.find('.menu');
      var scrollY = $(window).scrollTop();
      var docH = $(document).height();
      if (scrollY > _this.threshold) {
        _this.isActive = true;
        $nav.addClass('sticky');
        $navMenu.addClass('active');
        $nav.prev().css('margin-bottom', _this.hostTop + _this.hostH);
        // in case the menue containe is open
        $(_this.navContainer).removeClass('show');
        // $('.fab-goto-top').addClass('show')
      } else {
        _this.isActive = false;
        $nav.removeClass('sticky');
        $navMenu.removeClass('active');
        $nav.prev().css('margin-bottom', 0);
        // $('.fab-goto-top').removeClass('show')
      }
      // ## the drawer
      // this._updateFooterTop()
      // let scrolling = scrollY + ( windowH * 1.5 )
      // console.log(				'scoll y: ' + scrolling + ' || footerTop: ' + this.footerTop			)
      // if ( scrolling >= this.footerTop ){
      // 	$('.fab-drawer').addClass('show')
      // }else{
      // 	$('.fab-drawer').removeClass('show')
      // }
    });
    // menu
    $(this.navHeader).find('#active-menu').on('click', function() {
      if (_this.isActive) {
        $(_this.navContainer).toggleClass('show');
      }
    });
  };
  return PAStickyMenu;
}());
$(document).ready(function() {
  var kplayer = new KPlayer();
  var sticky = new PAStickyMenu('sticky-nav');
  // Kaltura API to pause video when closing the window
  kWidget.addReadyCallback(function( playerId ){
    var kdp = document.getElementById(playerId);
    $('#theater #theater-btn-close').on('click', function(){
      kdp.sendNotification("doPause");
    });
  });
});
