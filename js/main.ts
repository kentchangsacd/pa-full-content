class KPlayer {
	
	nameList: any = {}
	
	constructor(){
		// $('#theater').hide()
		// console.log("loaded")
		let targetVideos = $('.video-container')
		let triggers = $('.video-thumb')
		// ## create abjects
		for(let item of targetVideos){
			let vid = $(item).attr('vid')
			this.nameList[vid] = $(item)[0]
		}
		// console.log(this.nameList)
		// ## assign actions
		for(let i=0; i<triggers.length; i++){
			$(triggers[i]).on('click', ()=>{
				let vid = $(triggers[i]).attr('vid')
				$(this.nameList[vid]).addClass('active')
				// $('#theater').addClass('show')
				$('#theater').fadeIn('slow')
				// console.log("clicked" + vid)
			})
		}
		// ## assign closing actions
		$('#theater #theater-btn-close').on('click', function(){
			$('#theater').find('.active').removeClass('active')
			$('#theater').fadeOut('slow')
		})
	}
}


class PAStickyMenu {
  
  isActive: boolean = false
  host: any
  navHeader: any
  navContainer: any
  hostTop: number
	footerTop: number
  hostH: number
  threshold: number
  
  constructor(targetID: string){
    this.host = $('#'+targetID)
    this.navHeader = this.host.find('.sticky-nav')
    this.navContainer = this.host.find('.nav-container')
    this.hostTop = this.host.position().top
    this.hostH = this.host.height()
    this.threshold = this.hostTop + this.hostH
    // console.log(this.hostTop + ' / ' + this.hostH)
		let footer = $('footer')
		if (footer.length > 0){
			this.footerTop = $(footer).position().top
		}
    this._assignActions()
  }
	
	_updateFooterTop(){
		let footer = $('footer')
		this.footerTop = $(footer).position().top
	}
  
  _assignActions(){
    // scrolling
		let windowH = $(window).height()
    $(window).scroll(()=>{
      let $nav = $(this.host)
      let $navMenu = $nav.find('.menu')
      let scrollY = $(window).scrollTop()
			let docH = $(document).height()
      if (scrollY > this.threshold){
        this.isActive = true
        $nav.addClass('sticky')
        $navMenu.addClass('active')
        $nav.prev().css('margin-bottom', this.hostTop+this.hostH)
        // in case the menue containe is open
        $(this.navContainer).removeClass('show')
				// $('.fab-goto-top').addClass('show')
      }else{
        this.isActive = false
        $nav.removeClass('sticky')
        $navMenu.removeClass('active')
        $nav.prev().css('margin-bottom', 0)
				// $('.fab-goto-top').removeClass('show')
      }
			// ## the drawer
			// this._updateFooterTop()
			// let scrolling = scrollY + ( windowH * 1.5 )
			// console.log(				'scoll y: ' + scrolling + ' || footerTop: ' + this.footerTop			)
			// if ( scrolling >= this.footerTop ){
			// 	$('.fab-drawer').addClass('show')
			// }else{
			// 	$('.fab-drawer').removeClass('show')
			// }
    })
    // menu
    $(this.navHeader).find('#active-menu').on('click', ()=>{
      if (this.isActive){
        $(this.navContainer).toggleClass('show')
      }
    })
  }
  
}
	
$(document).ready(function(){
		let kplayer = new KPlayer()
		let sticky = new PAStickyMenu('sticky-nav')
}