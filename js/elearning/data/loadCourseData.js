// JavaScript Document
function loadCourseData(xml) {
				$(xml).find('banner').each(function(){
					var courseTitle = $(this).find('courseTitle').text();
					$('h1.title').append(courseTitle);
				});
				$(xml).find('unit[week="01"]').each(function(){
					var unitTitle = $(this).find('introduction').children('title').text();
					var topicOne = $(this).find('topic[order="1"]').children().text();
					$('section h1').append(' ' +unitTitle);
					$('li#topic').find('li:first').children().append('1. ' +topicOne);
					
				});
			};