<%@ page contentType="text/html; charset=UTF-8" language="java" extends = "edu.scad.elearning.contentserver.servlet.BaseJspServlet" import="java.sql.*" errorPage="" %>

    <!doctype html>
    <html class="no-js" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>P/A - Landing Page</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/main.css">
        <script src="../js/vendor/modernizr-2.8.3.min.js"></script>

        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="../css/materialize.min.css">
        <link rel="stylesheet" href="../css/landing.processed.css" />

        <script src="../js/vendor/jquery-1.12.0.min.js"></script>

        <!-- Compiled and minified JavaScript -->
        <script src="../js/vendor/materialize.min.js"></script>
        <script>
            window.jQuery || document.write('<script src="../js/vendor/jquery-1.12.0.min.js"><\/script>')
        </script>

        <script type="text/javascript" src="../scarystuff/js/extensions/data/loadCourseData.js"></script>
        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $.ajax({
                    type: "GET",
                    url: "../scarystuff/xml/courseData.xml",
                    dataType: "xml",
                    success: loadCourseData
                });
            });
        </script>


    </head>

    <body>
        <!--[if lt IE 8]>
          <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
      <![endif]-->

        <div class='wrapper'>
            <header class="header valign-wrapper">
                <div class='header-bg'>
                    <img src="../assets/landing-images/banner-syllabus.svg" alt="" />
                </div>
                <div class='course-text'>
                    <div class="course-leading">
                        <div class="dept-title">Industrial Design</div>
                        <div class="school-title">School of Design</div>
                        <div class="course-id">PSYO 000</div>
                    </div>
                    <div class="course-title">
                        <!-- 						<h1>Psycho Aesthetics:</h1>
          <h2>Advanced Design-thingking Methodology</h2>	 -->
                        <div class="logo-container">
                            <img src="../assets/icons/logo-main.svg" alt="P/A by RKS title logo: Advanced Design-thinking methodology.">
                        </div>
                    </div>
                </div>
                <div class='logo'>
                    <img src="../assets/icons/rks-scad-white.svg" alt="RKS and SCAD logo">
                </div>
            </header>

            <!-- 	## Units -->

            <ul class="collapsible popout pa-accordion" data-collapsible="accordion">

                <li>
                    <div class="collapsible-header valign-wrapper">
                        <div class="unit-icon">
                            <img src="../assets/icons/nav-1-midicon.svg" alt="">
                        </div>
                        <div class="header-text">
                            <div class="unit-number">Unit 1</div>
                            <div class="unit-title">Core Principles of Psycho-Aesthetics</div>
                        </div>
                        <div class="unit-date">
                            <%=getFrom(1, request)%> &ndash;
                                <%=getTo(1, request)%>
                        </div>
                        <a class="btn-floating halfway-fab waves-effect waves-light hoverable tooltipped" data-position="top" data-delay="50" data-tooltip="GO TO UNIT" href="#" onClick="popTheme('../unit-01/unit-page.html', 'Unit 1: Core Princeples of Psycho-Aesthetics')">
                            <i class="material-icons">arrow_forward</i>
                        </a>
                        <div class="bg-img">
                            <img src="../assets/landing-images/unit-1.jpg" alt="Unit 1 cover image">
                        </div>
                    </div>
                    <div class="collapsible-body">
                        <p>In this first unit, you will look at the importance of a solid design process, the core components and ideology that comprise Psycho-Aesthetics, and why research is critical in creating a successful design outcome. </p>
                        <div class="right-align">
                            <a href="#" onClick="popTheme('../unit-01/assi.html', 'Unit 1: Core Princeples of Psycho-Aesthetics')">Assignments</a>
                            <a href="#" onClick="popTheme('../unit-01/project.html', 'Unit 1: Core Princeples of Psycho-Aesthetics')">Projects</a>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="collapsible-header valign-wrapper">
                        <div class="unit-icon">
                            <img src="../assets/icons/nav-2-midicon.svg" alt="">
                        </div>
                        <div class="header-text">
                            <div class="unit-number">Unit 2</div>
                            <div class="unit-title">Shifting Through Contextual Research</div>
                        </div>
                        <div class="unit-date">
                            <%=getFrom(2, request)%> &ndash;
                                <%=getTo(2, request)%>
                        </div>
                        <a class="btn-floating halfway-fab waves-effect waves-light hoverable tooltipped" data-position="top" data-delay="50" data-tooltip="GO TO UNIT" href="#" onClick="popTheme('../unit-02/unit-page.html', 'Unit 2: Shifting Through Contextual Research')">
                            <i class="material-icons">arrow_forward</i>
                        </a>
                        <div class="bg-img">
                            <img src="../assets/landing-images/unit-2.jpg" alt="Unit 2 cover image">
                        </div>
                    </div>
                    <div class="collapsible-body">
                        <p>In this unit, you will analyze and synthesize your research to develop personas, identify key triggers, and create &ldquo;help-me&rdquo; statements that will guide the design process.</p>
                        <div class="right-align">
                            <a href="#" onClick="popTheme('../unit-02/assi.html', 'Unit 2: Shifting Through Contextual Research')">Assignments</a>
                            <a href="#" onClick="popTheme('../unit-02/project.html', 'Unit 2: Shifting Through Contextual Research')">Projects</a>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="collapsible-header valign-wrapper">
                        <div class="unit-icon">
                            <img src="../assets/icons/nav-3-midicon.svg" alt="">
                        </div>
                        <div class="header-text">
                            <div class="unit-number">Unit 3</div>
                            <div class="unit-title">The P/A Map</div>
                        </div>
                        <div class="unit-date">
                            <%=getFrom(3, request)%> &ndash;
                                <%=getTo(3, request)%>
                        </div>
                        <a class="btn-floating halfway-fab waves-effect waves-light hoverable tooltipped" data-position="top" data-delay="50" data-tooltip="GO TO UNIT" href="#" onClick="popTheme('../unit-03/unit-page.html', 'Unit 3: The P/A Map')">
                            <i class="material-icons">arrow_forward</i>
                        </a>
                        <div class="bg-img">
                            <img src="../assets/landing-images/unit-3.jpg" alt="Unit 3 cover image">
                        </div>
                    </div>
                    <div class="collapsible-body">
                        <p>In this unit, you will learn how to use a cornerstone of the Psycho-Aesthetics process: the P/A map. The map will enable you to visualize your research in a way that fosters creativity and brings focus to the design process.</p>
                        <div class="right-align">
                            <a href="#" onClick="popTheme('../unit-03/assi.html', 'Unit 3: The P/A Map')">Assignments</a>
                            <a href="#" onClick="popTheme('../unit-03/project.html', 'Unit 3: The P/A Map')">Projects</a>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="collapsible-header valign-wrapper">
                        <div class="unit-icon">
                            <img src="../assets/icons/nav-4-midicon.svg" alt="">
                        </div>
                        <div class="header-text">
                            <div class="unit-number">Unit 4</div>
                            <div class="unit-title">The Hero's Journey</div>
                        </div>
                        <div class="unit-date">
                            <%=getFrom(4, request)%> &ndash;
                                <%=getTo(4, request)%>
                        </div>
                        <a class="btn-floating halfway-fab waves-effect waves-light hoverable tooltipped" data-position="top" data-delay="50" data-tooltip="GO TO UNIT" href="#" onClick="popTheme('../unit-04/unit-page.html', 'Unit 4: The Heros Journey')">
                            <i class="material-icons">arrow_forward</i>
                        </a>
                        <div class="bg-img">
                            <img src="../assets/landing-images/unit-4.jpg" alt="Unit 4 cover image">
                        </div>
                    </div>
                    <div class="collapsible-body">
                        <p>In this unit, you will learn about the core elements of the Hero&rsquo;s Journey and how to develop a narrative for your design. </p>
                        <div class="right-align">
                            <a href="#" onClick="popTheme('../unit-04/assi.html', 'Unit 4: The Heros Journey')">Assignments</a>
                            <a href="#" onClick="popTheme('../unit-04/project.html', 'Unit 4: The Heros Journey')">Projects</a>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="collapsible-header valign-wrapper">
                        <div class="unit-icon">
                            <img src="../assets/icons/nav-5-midicon.svg" alt="">
                        </div>
                        <div class="header-text">
                            <div class="unit-number">Unit 5</div>
                            <div class="unit-title">Process Presentation</div>
                        </div>
                        <div class="unit-date">
                            <%=getFrom(5, request)%> &ndash;
                                <%=getTo(5, request)%>
                        </div>
                        <a class="btn-floating halfway-fab waves-effect waves-light hoverable tooltipped" data-position="top" data-delay="50" data-tooltip="GO TO UNIT" href="#" onClick="popTheme('../unit-05/unit-page.html', 'Unit 5: Process Presentation')">
                            <i class="material-icons">arrow_forward</i>
                        </a>
                        <div class="bg-img">
                            <img src="../assets/landing-images/unit-5.jpg" alt="Unit 5 cover image">
                        </div>
                    </div>
                    <div class="collapsible-body">
                        <p>In this unit, you will discover how the P/A process enables you to powerfully communicate the value of your design decisions with various stakeholders, from clients to other collaborators. </p>
                        <div class="right-align">
                            <a href="#" onClick="popTheme('../unit-05/assi.html', 'Unit 5: Process Presentation')">Assignments</a>
                            <a href="#" onClick="popTheme('../unit-05/project.html', 'Unit 5: Process Presentation')">Projects</a>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="collapsible-header valign-wrapper">
                        <div class="unit-icon">
                            <img src="../assets/icons/nav-6-midicon.svg" alt="">
                        </div>
                        <div class="header-text">
                            <div class="unit-number">Unit 6</div>
                            <div class="unit-title">Laying the Foundation</div>
                        </div>
                        <div class="unit-date">
                            <%=getFrom(6, request)%> &ndash;
                                <%=getTo(6, request)%>
                        </div>
                        <a class="btn-floating halfway-fab waves-effect waves-light hoverable tooltipped" data-position="top" data-delay="50" data-tooltip="GO TO UNIT" href="#" onClick="popTheme('../unit-06/unit-page.html', 'Unit 6: Laying the Foundation')">
                            <i class="material-icons">arrow_forward</i>
                        </a>
                        <div class="bg-img">
                            <img src="../assets/landing-images/unit-6.jpg" alt="Unit 6 cover image">
                        </div>
                    </div>
                    <div class="collapsible-body">
                        <p>In this unit, you will engage with the P/A process individually starting with contextual research. You will also learn how P/A can be applied specifically to product design. </p>
                        <div class="right-align">
                            <a href="#" onClick="popTheme('../unit-06/assi.html', 'Unit 6: Laying the Foundation')">Assignments</a>
                            <a href="#" onClick="popTheme('../unit-06/project.html', 'Unit 6: Laying the Foundation')">Projects</a>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="collapsible-header valign-wrapper">
                        <div class="unit-icon">
                            <img src="../assets/icons/nav-7-midicon.svg" alt="">
                        </div>
                        <div class="header-text">
                            <div class="unit-number">Unit 7</div>
                            <div class="unit-title">Interpreting Contextual Research</div>
                        </div>
                        <div class="unit-date">
                            <%=getFrom(7, request)%> &ndash;
                                <%=getTo(7, request)%>
                        </div>
                        <a class="btn-floating halfway-fab waves-effect waves-light hoverable tooltipped" data-position="top" data-delay="50" data-tooltip="GO TO UNIT" href="#" onClick="popTheme('../unit-07/unit-page.html', 'Unit 7: Interpreting Contextual Research')">
                            <i class="material-icons">arrow_forward</i>
                        </a>
                        <div class="bg-img">
                            <img src="../assets/landing-images/unit-7.jpg" alt="Unit 7 cover image">
                        </div>
                    </div>
                    <div class="collapsible-body">
                        <p>In this unit, you will analyze and synthesize your individual research to develop personals, identify key triggers, and create &ldquo;help-me&rdquo; statements. In addition, you will learn how P/A can be used to guide the creation
                            of services.</p>
                        <div class="right-align">
                            <a href="#" onClick="popTheme('../unit-07/assi.html', 'Unit 7: Interpreting Contextual Research')">Assignments</a>
                            <a href="#" onClick="popTheme('../unit-07/project.html', 'Unit 7: Interpreting Contextual Research')">Projects</a>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="collapsible-header valign-wrapper">
                        <div class="unit-icon">
                            <img src="../assets/icons/nav-8-midicon.svg" alt="">
                        </div>
                        <div class="header-text">
                            <div class="unit-number">Unit 8</div>
                            <div class="unit-title">Visualizing Research</div>
                        </div>
                        <div class="unit-date">
                            <%=getFrom(8, request)%> &ndash;
                                <%=getTo(8, request)%>
                        </div>
                        <a class="btn-floating halfway-fab waves-effect waves-light hoverable tooltipped" data-position="top" data-delay="50" data-tooltip="GO TO UNIT" href="#" onClick="popTheme('../unit-08/unit-page.html', 'Unit 8: Visualizing Research')">
                            <i class="material-icons">arrow_forward</i>
                        </a>
                        <div class="bg-img">
                            <img src="../assets/landing-images/unit-8.jpg" alt="Unit 8 cover image">
                        </div>
                    </div>
                    <div class="collapsible-body">
                        <p>In this unit, you will map your findings and determine the trajectory of the market and your personas. You will also learn how P/A can be applied to all aspects of branding, from company colors to logos. </p>
                        <div class="right-align">
                            <a href="#" onClick="popTheme('../unit-08/assi.html', 'Unit 8: Visualizing Research')">Assignments</a>
                            <a href="#" onClick="popTheme('../unit-08/project.html', 'Unit 8: Visualizing Research')">Projects</a>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="collapsible-header valign-wrapper">
                        <div class="unit-icon">
                            <img src="../assets/icons/nav-9-midicon.svg" alt="">
                        </div>
                        <div class="header-text">
                            <div class="unit-number">Unit 9</div>
                            <div class="unit-title">Testing Via Storytelling</div>
                        </div>
                        <div class="unit-date">
                            <%=getFrom(9, request)%> &ndash;
                                <%=getTo(9, request)%>
                        </div>
                        <a class="btn-floating halfway-fab waves-effect waves-light hoverable tooltipped" data-position="top" data-delay="50" data-tooltip="GO TO UNIT" href="#" onClick="popTheme('../unit-09/unit-page.html', 'Unit 9: Testing Via Storytelling')">
                            <i class="material-icons">arrow_forward</i>
                        </a>
                        <div class="bg-img">
                            <img src="../assets/landing-images/unit-9.jpg" alt="Unit 9 cover image">
                        </div>
                    </div>
                    <div class="collapsible-body">
                        <p>In this unit, you will validate your design by running it through the Hero&rsquo;s Journey. You will also learn how P/A can be used on a large scale to design companies.</p>
                        <div class="right-align">
                            <a href="#" onClick="popTheme('../unit-09/assi.html', 'Unit 9: Testing Via Storytelling')">Assignments</a>
                            <a href="#" onClick="popTheme('../unit-09/project.html', 'Unit 9: Testing Via Storytelling')">Projects</a>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="collapsible-header valign-wrapper">
                        <div class="unit-icon">
                            <img src="../assets/icons/unit-10-objectives-after.svg" alt="">
                        </div>
                        <div class="header-text">
                            <div class="unit-number">Unit 10</div>
                            <div class="unit-title">Psycho-Aesthetics and Future Endeavors</div>
                        </div>
                        <div class="unit-date">
                            <%=getFrom(10, request)%> &ndash;
                                <%=getTo(10, request)%>
                        </div>
                        <a class="btn-floating halfway-fab waves-effect waves-light hoverable tooltipped" data-position="top" data-delay="50" data-tooltip="GO TO UNIT" href="#" onClick="popTheme('../unit-10/unit-page.html', 'Unit 10: Psycho-Aesthetics and Future Endeavors')">
                            <i class="material-icons">arrow_forward</i>
                        </a>
                        <div class="bg-img">
                            <img src="../assets/landing-images/unit-10.jpg" alt=" Unit 10 cover iamge">
                        </div>
                    </div>
                    <div class="collapsible-body">
                        <p>In this unit, you will prepare your work for final presentation and review. You will reflect on the hard work you have done and look ahead toward your future goals as a designer. </p>
                        <div class="right-align">
                            <a href="#" onClick="popTheme('../unit-10/assi.html', 'Unit 10: Psycho-Aesthetics and Future Endeavors')">Assignments</a>
                            <a href="#" onClick="popTheme('../unit-10/project.html', 'Unit 10: Psycho-Aesthetics and Future Endeavors')">Projects</a>
                        </div>
                    </div>
                </li>
            </ul>


            <footer class="footer valign-wrapper">
                <div class="footer-logo center-algin">
                    <img src="../assets/icons/rks-scad-white.svg" alt="RKS and SCAD logo.">
                </div>
            </footer>



            <script>
                $('.pa-accordion li').on('click', function() {
                    window.setTimeout(function() {
                        parent.iframeResize();
                    }, 500);
                });

                function popTheme(url, title) {
                    // Fixes dual-screen position                         Most browsers      Firefox
                    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
                    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

                    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                    var left = 200 + dualScreenLeft;
                    var top = 0 + dualScreenTop;

                    var w;
                    (width <= 1280) ? w = 1280: w = width - left;
                    // var w = width - left;
                    var h = height;

                    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

                    // Puts focus on the newWindow
                    if (window.focus) {
                        newWindow.focus();
                    }
                }

                $(document).ready(function() {
                    // var name = $('html').parent().parent();
                    // name.css('height', '3000px')
                    // console.log(name);
                    var minWidth = 500;
                    var docH = 3000;
                    // console.log($(window).width());
                    if ($(window).width() >= minWidth) {
                        window.parent.document.getElementById('contentIframe').style.height = '100vh';
                        window.parent.document.getElementById('contentIframe').height = '100vh';
                    } else {
                        window.parent.document.getElementById('contentIframe').style.height = docH + 'px';
                        window.parent.document.getElementById('contentIframe').height = docH + 'px';
                    }
                    // console.log(name);
                }, 3000);
            </script>
            <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
            <script>
                (function(b, o, i, l, e, r) {
                    b.GoogleAnalyticsObject = l;
                    b[l] || (b[l] =
                        function() {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                    b[l].l = +new Date;
                    e = o.createElement(i);
                    r = o.getElementsByTagName(i)[0];
                    e.src = 'https://www.google-analytics.com/analytics.js';
                    r.parentNode.insertBefore(e, r)
                }(window, document, 'script', 'ga'));
                ga('create', 'UA-XXXXX-X', 'auto');
                ga('send', 'pageview');
            </script>
    </body>

    </html>