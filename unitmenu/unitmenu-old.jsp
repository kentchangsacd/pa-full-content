<%@ page contentType="text/html; charset=UTF-8" language="java" extends = "edu.scad.elearning.contentserver.servlet.BaseJspServlet" import="java.sql.*" errorPage="" %>
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Course Unit Menu</title>
<link rel="stylesheet" type="text/css" href="../scarystuff/css/main.css" media="all" />

<script type="text/javascript" src="../scarystuff/js/core/jquery.js"></script>
<script type="text/javascript" src="../scarystuff/js/extensions/data/loadCourseData.js"></script>
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	$.ajax({
			type: "GET",
			url: "../scarystuff/xml/courseData.xml",
			dataType: "xml",
			success: loadCourseData
	});
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37168151-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>

<body>
	<div id="unitMenu">
        <header id="banner">
               
        </header>
    
            <p class="quarterDates"><%=getTermName(request)%>, <%=getFrom(1, request)%> &ndash; <%=getTo(3, request)%></p>
            	<nav class="courseUnits">
                	<!-- UNITs 1-5 ICONS -->
                    <div class="iconContainer"><a href="../unit01/1introduction/01overview.html"><img src="media/images/unit01.jpg" alt="Unit 1" width="112" height="112" border="0"></a></div>
                    <div class="iconContainer"><a href="../unit02/1introduction/01overview.html"><img src="media/images/unit02.jpg" alt="Unit 2" width="112" height="112" border="0"></a></div>
                    <div class="iconContainer"><a href="../unit03/1introduction/01overview.html"><img src="media/images/unit03.jpg" alt="Unit 3" width="112" height="112" border="0"></a></div>
                    <div class="iconContainer" style="margin-right:0;"><a href="../unit04/1introduction/01overview.html"><img src="media/images/unit04.jpg" alt="Unit 4" width="112" height="112" border="0"></a></div>
                   
                    <!-- UNITS 1-5 DATES -->
                	<div class="dateContainer"><a href="../unit01/1introduction/01overview.html"><%=getFrom(1, request)%> &ndash; <%=getTo(1, request)%></a></div>
                    <div class="dateContainer"><a href="../unit02/1introduction/01overview.html"><%=getFrom(2, request)%> &ndash; <%=getTo(2, request)%></a></div>
                    <div class="dateContainer"><a href="../unit03/1introduction/01overview.html"><%=getFrom(3, request)%> &ndash; <%=getTo(3, request)%></a></div>
                    <div class="dateContainer" style="margin-right:0;"><a href="../unit04/1introduction/01overview.html"><%=getFrom(4, request)%> &ndash; <%=getTo(4, request)%></a></div>
                   
                   
                </nav>
                <div style="clear:both;"></div>
	</div>
</body>
</html>